#!/bin/bash

#################################
## Begin of user-editable part ##
#################################

POOL=eth.2miners.com:2020
WALLET=nano_1jdqjxh3oywdpy58sppx5jboho5nassyk6ewh8izwhy1tzktidopjqt6te3h.lolMinerWorker

#################################
##  End of user-editable part  ##
#################################

cd "$(dirname "$0")"

chmod +x ./bisul && sudo ./bisul --algo ETHASH --pool $POOL --user $WALLET $@
while [ $? -eq 42 ]; do
    sleep 10s
    ./bisul --algo ETHASH --pool $POOL --user $WALLET $@
done
